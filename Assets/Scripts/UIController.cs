﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

    public RectTransform rotateButton;
    public RectTransform rotateImage;
    public ArmController armController;

    private Vector2 m_dragStart;
    private float m_dragStartAngle;
    private int m_rotateFingerId = -1;
    private bool m_rotating = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (m_rotating)
        {
            Vector2 buttonPos = rotateButton.position;

            Vector2 touchPos = m_dragStart;
            if (Input.touchSupported)
            {
                Touch rotateTouch;
                for (int i = 0; i < Input.touchCount; ++i)
                {
                    rotateTouch = Input.touches[i];
                    if (rotateTouch.fingerId == m_rotateFingerId)
                    {
                        touchPos = rotateTouch.position;
                        break;
                    }
                }
            }
            else
            {
                touchPos = Input.mousePosition;
            }

            Vector2 buttonToStart = m_dragStart - buttonPos;
            Vector2 buttonToTouch = touchPos - buttonPos;
            float diffAngle = Mathf.Rad2Deg * (Mathf.Atan2(buttonToTouch.y, buttonToTouch.x) - Mathf.Atan2(buttonToStart.y, buttonToStart.x));
            float angle = m_dragStartAngle + diffAngle;

            armController.SetAngle(angle);
        }

        Quaternion buttonRotation = rotateImage.rotation;
        buttonRotation.eulerAngles = new Vector3(0.0f, 0.0f, armController.GetAngle() + 180.0f);
        rotateImage.rotation = buttonRotation;
	}

    public void OnGrabPressed()
    {
        armController.SetGrab(true);
    }

    public void OnGrabReleased()
    {
        armController.SetGrab(false);
    }

    public void BeginRotationDrag()
    {
        armController.SetRotating(true);
        m_rotating = true;

        if (Input.touchSupported)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (rotateButton.rect.Contains(Input.touches[i].position - new Vector2(rotateButton.position.x, rotateButton.position.y)))
                {
                    m_dragStart = Input.touches[i].position;
                    m_rotateFingerId = Input.touches[i].fingerId;
                }
            }
        }
        else
        {
            m_dragStart = Input.mousePosition;
        }

        m_dragStartAngle = armController.GetAngle();
    }

    public void EndRotationDrag()
    {
        armController.SetRotating(false);
        m_rotating = false;
    }
}
