﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ShowEdgeColliders : MonoBehaviour
{
    public Color color;

    // The Collider itself
    private PolygonCollider2D thisCollider;
    // array of collider points
    private Vector2[] points;
    // the transform position of the collider
    private Matrix4x4 _t;

    void Start()
    {
        
        
    }

    void OnDrawGizmos()
    {
        thisCollider = GetComponent<PolygonCollider2D>();
        points = thisCollider.points;
        _t = transform.localToWorldMatrix;
        

        Gizmos.color = color;
        // for every point (except for the last one), draw line to the next point
        for (int i = 0; i < points.Length - 1; i++)
        {
            Gizmos.DrawLine((_t * new Vector3(points[i].x, points[i].y)) + new Vector4(transform.position.x, transform.position.y, transform.position.z, 0.0f), (_t * new Vector3(points[i + 1].x, points[i + 1].y)) + new Vector4(transform.position.x, transform.position.y, transform.position.z, 0.0f));
        }
        Gizmos.DrawLine(_t * new Vector3(points[points.Length - 1].x, points[points.Length - 1].y) + new Vector4(transform.position.x, transform.position.y, transform.position.z, 0.0f), _t * new Vector3(points[0].x, points[0].y) + new Vector4(transform.position.x, transform.position.y, transform.position.z, 0.0f));
    }

}