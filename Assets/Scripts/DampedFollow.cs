﻿using UnityEngine;
using System.Collections;

public class DampedFollow : MonoBehaviour {

    public Transform target;
    public float acceleration = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 pos = new Vector2(transform.position.x, transform.position.y);
        Vector2 targetPos = new Vector2(target.transform.position.x, target.transform.position.y);
        pos = targetPos * acceleration + (1.0f - acceleration) * pos;

        transform.position = new Vector3(pos.x, pos.y, transform.position.z);
	}
}
