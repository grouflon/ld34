﻿using UnityEngine;
using System.Collections;

public class KeyboardController : MonoBehaviour {

    public ArmController leftArmController;
    public ArmController rightArmController;

    public RectTransform leftRotateImage;
    public RectTransform rightRotateImage;
    public float rotateSpeed = 30.0f;

	// Use this for initialization
	void Start () {
        if (Input.touchSupported)
        {
            enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("LeftArmRotateLeft")) // Left rotate down
        {
            leftArmController.SetRotating(true);
            float angle = leftArmController.GetAngle();
            angle += rotateSpeed * Time.deltaTime;
            leftArmController.SetAngle(angle);
        }

        if (Input.GetButton("LeftArmRotateRight")) // Left rotate up
        {
            leftArmController.SetRotating(true);
            float angle = leftArmController.GetAngle();
            angle -= rotateSpeed * Time.deltaTime;
            leftArmController.SetAngle(angle);
        }

        if (Input.GetButtonUp("LeftArmRotateLeft") || Input.GetButtonUp("LeftArmRotateRight"))
        {
            leftArmController.SetRotating(false);
        }

        if (Input.GetButton("LeftArmGrab")) // Left Grab
        {
            leftArmController.SetGrab(true);
        }
        else
        {
            leftArmController.SetGrab(false);
        }



        if (Input.GetButton("RightArmRotateLeft")) // Right rotate down
        {
            rightArmController.SetRotating(true);
            float angle = rightArmController.GetAngle();
            angle += rotateSpeed * Time.deltaTime;
            rightArmController.SetAngle(angle);
        }

        if (Input.GetButton("RightArmRotateRight")) // Right rotate up
        {
            rightArmController.SetRotating(true);
            float angle = rightArmController.GetAngle();
            angle -= rotateSpeed * Time.deltaTime;
            rightArmController.SetAngle(angle);
        }

        if (Input.GetButtonUp("RightArmRotateLeft") || Input.GetButtonUp("RightArmRotateRight"))
        {
            rightArmController.SetRotating(false);
        }

        if (Input.GetButton("RightArmGrab")) // Right Grab
        {
            rightArmController.SetGrab(true);
        }
        else
        {
            rightArmController.SetGrab(false);
        }
	}
}
