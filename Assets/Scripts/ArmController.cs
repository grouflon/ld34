﻿using UnityEngine;
using System.Collections;

public class ArmController : MonoBehaviour {

    private float m_armAngle = 0.0f;
    private float m_grabModifier = 0.0f;
    private float m_rotatingModifier = 0.0f;
    private bool m_rotating = false;
    private bool m_grab = false;
    private bool m_grabLocked = false;
    private float angleAcceleration = 10.0f;
    private HingeJoint2D m_handHinge;

    public Rigidbody2D armRigidbody;
    public Rigidbody2D foreArmRigidbody;
    public HandController hand;
    public GameObject handTip;
    public Material grabMaterial;
    public Material nograbMaterial;

	// Use this for initialization
	void Start () {
        HingeJoint2D[] hingeJoints = armRigidbody.GetComponents<HingeJoint2D>();
        foreach (HingeJoint2D joint in hingeJoints)
        {
            if (joint.connectedBody == null)
            {
                m_handHinge = joint;
                break;
            }
        }
	}

    void Update()
    {
        handTip.GetComponent<Renderer>().material = m_grab ? grabMaterial : nograbMaterial;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (m_rotating && !m_grab)
        {
            m_handHinge.enabled = false;

            /*Quaternion armRotation = armRigidbody.transform.rotation;
            armRotation.eulerAngles = new Vector3(0.0f, 0.0f, m_armAngle);
            armRigidbody.transform.rotation = armRotation;*/

            float targetAngle = (m_armAngle + 360.0f) % 360.0f;
            float currentAngle = (armRigidbody.transform.rotation.eulerAngles.z + 360.0f) % 360.0f;

            while (targetAngle - currentAngle > 180.0f)
            {
                targetAngle -= 360.0f;
            }

            while (currentAngle - targetAngle> 180.0f)
            {
                targetAngle += 360.0f;
            }

            armRigidbody.angularVelocity = (targetAngle - currentAngle) * angleAcceleration;
        }
        else if (!m_rotating && m_grab )
        {
            m_handHinge.enabled = true;
        }
        else if (m_rotating && m_grab)
        {
            m_handHinge.enabled = true;
           
            /*Quaternion armRotation = foreArmRigidbody.transform.rotation;
            armRotation.eulerAngles = new Vector3(0.0f, 0.0f, m_armAngle + m_grabModifier + m_rotatingModifier);
            foreArmRigidbody.transform.rotation = armRotation;*/

            float targetAngle = (m_armAngle + m_grabModifier + m_rotatingModifier + 360.0f) % 360.0f;
            float currentAngle = (foreArmRigidbody.transform.rotation.eulerAngles.z + 360.0f) % 360.0f;

            while (targetAngle - currentAngle > 180.0f)
            {
                targetAngle -= 360.0f;
            }

            while (currentAngle - targetAngle > 180.0f)
            {
                targetAngle += 360.0f;
            }

            foreArmRigidbody.angularVelocity = (targetAngle - currentAngle) * angleAcceleration * 3.0f;
        }
        else
        {
            m_handHinge.enabled = false;

            m_armAngle = armRigidbody.transform.rotation.eulerAngles.z;
        }
	}

    public void SetAngle(float _rotation)
    {
        if (!m_rotating)
            return;

        m_armAngle = _rotation;
    }

    public float GetAngle()
    {
        return m_armAngle;
    }

    public void SetGrab(bool _grab)
    {
        if (_grab)
        {
            if (m_grabLocked)
                return;

            if (!hand.CanGrab())
            {
                m_grabLocked = true;
                return;
            }
        }
        else
        {
            m_grabLocked = false;
        }

        if (_grab == m_grab)
            return;

        m_grab = _grab;
        if (m_grab)
        {
            m_handHinge.connectedAnchor = new Vector2(hand.transform.position.x, hand.transform.position.y);
        }

        if (m_rotating && m_grab)
        {
            m_grabModifier = foreArmRigidbody.transform.rotation.eulerAngles.z - m_armAngle;
        }
        else
        {
            m_grabModifier = 0.0f;
        }
    }

    public bool GetGrab()
    {
        return m_grab;
    }

    public void SetRotating(bool _rotating)
    {
        if (_rotating == m_rotating)
            return;

        m_rotating = _rotating;

        if (m_rotating && m_grab)
        {
            m_rotatingModifier = foreArmRigidbody.transform.rotation.eulerAngles.z - m_armAngle;
        }
        else
        {
            m_rotatingModifier = 0.0f;
        }
    }

    public bool GetRotating()
    {
        return m_rotating;
    }
}
