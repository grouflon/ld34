﻿using UnityEngine;
using System.Collections;

public class HandController : MonoBehaviour {

    private bool m_canGrab = false;

    public bool CanGrab()
    {
        return m_canGrab;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        m_canGrab = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        m_canGrab = false;
    }
}
