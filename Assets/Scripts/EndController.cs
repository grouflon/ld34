﻿using UnityEngine;
using System.Collections;

public class EndController : MonoBehaviour {

    public GameObject particles;
    public GameObject playerBody;
    public Animator gameAnimator;

    private bool m_end = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (m_end)
        {
            gameAnimator.SetBool("end", true);
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == playerBody)
        {
            m_end = true;
            particles.SetActive(true);
        }
    }
}
