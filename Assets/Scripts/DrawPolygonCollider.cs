﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(PolygonCollider2D))]
//[RequireComponent(typeof(LineRenderer))]

public class DrawPolygonCollider : MonoBehaviour {

    private MeshFilter m_meshFilter;
    private PolygonCollider2D m_collider;
    //private LineRenderer m_lineRenderer;
    private Mesh m_mesh;

	// Use this for initialization
	void Start () {
        m_meshFilter = GetComponent<MeshFilter>();
        m_collider = GetComponent<PolygonCollider2D>();
   //     m_lineRenderer = GetComponent<LineRenderer>();
        m_mesh = new Mesh();
    //    _BuildLine();
        _BuildMesh();
	}
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    private void _BuildMesh()
    {
        m_mesh.Clear();

        CombineInstance[] combine = new CombineInstance[m_collider.pathCount];
        for (int i = 0; i < m_collider.pathCount; ++i)
        {
            Mesh pathMesh = new Mesh();
            Vector2[] points = m_collider.GetPath(i);
            Vector3[] vertices = new Vector3[points.Length];

            for (int j = 0; j < points.Length; ++j)
            {
                vertices[j] = new Vector3(points[j].x, points[j].y, 0.0f);
            }

            Triangulator triangulator = new Triangulator(points);
            int[] triangles = triangulator.Triangulate();

            pathMesh.vertices = vertices;
            pathMesh.triangles = triangles;
            combine[i].mesh = pathMesh;
            combine[i].transform = Matrix4x4.identity;
            //m_mesh = pathMesh;
        }

        m_mesh.CombineMeshes(combine);
        m_mesh.RecalculateBounds();
        m_mesh.RecalculateNormals();
        m_meshFilter.mesh = m_mesh;
    }

    /*private void _BuildLine()
    {
        Vector3 position = transform.position;
        int pointCount = m_collider.GetTotalPointCount();
        m_lineRenderer.SetVertexCount(pointCount + 1);
        for (int i = 0; i < pointCount; ++i)
        {
            Vector2 startPoint = m_collider.points[i];
            m_lineRenderer.SetPosition(i, new Vector3(startPoint.x, startPoint.y, 0.0f) + position);
        }
        Vector2 lastPoint = m_collider.points[0];
        m_lineRenderer.SetPosition(pointCount, new Vector3(lastPoint.x, lastPoint.y, 0.0f) + position);
        m_lineRenderer.SetColors(Color.blue, Color.blue);
    }*/
}
